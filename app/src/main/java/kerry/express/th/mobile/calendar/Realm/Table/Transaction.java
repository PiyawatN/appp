package kerry.express.th.mobile.calendar.Realm.Table;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by namwanta on 11/22/2016 AD.
 */

public class Transaction extends RealmObject {

    private String tran_where;
    private String tran_what;
    private String tran_how;
    private String tran_type;
    private String tran_amount;
    private String tran_distance;
    private Date tran_date;

    public String getTran_where() {
        return tran_where;
    }

    public void setTran_where(String tran_where) {
        this.tran_where = tran_where;
    }

    public String getTran_what() {
        return tran_what;
    }

    public void setTran_what(String tran_what) {
        this.tran_what = tran_what;
    }

    public String getTran_how() {
        return tran_how;
    }

    public void setTran_how(String tran_how) {
        this.tran_how = tran_how;
    }

    public String getTran_type() {
        return tran_type;
    }

    public void setTran_type(String tran_type) {
        this.tran_type = tran_type;
    }

    public String getTran_amount() {
        return tran_amount;
    }

    public void setTran_amount(String tran_amount) {
        this.tran_amount = tran_amount;
    }

    public String getTran_distance() {
        return tran_distance;
    }

    public void setTran_distance(String tran_distance) {
        this.tran_distance = tran_distance;
    }

    public Date getTran_date() {
        return tran_date;
    }

    public void setTran_date(Date tran_date) {
        this.tran_date = tran_date;
    }
}
