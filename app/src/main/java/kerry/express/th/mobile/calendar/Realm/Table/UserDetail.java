package kerry.express.th.mobile.calendar.Realm.Table;

import io.realm.RealmObject;

/**
 * Created by namwanta on 11/22/2016 AD.
 */

public class UserDetail extends RealmObject {

    private String user_imei;
    private String user_name;
    private String user_dep;
    private String user_pos;
    private String user_plateno;

    public String getUser_imei() {
        return user_imei;
    }

    public void setUser_imei(String user_imei) {
        this.user_imei = user_imei;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_dep() {
        return user_dep;
    }

    public void setUser_dep(String user_dep) {
        this.user_dep = user_dep;
    }

    public String getUser_pos() {
        return user_pos;
    }

    public void setUser_pos(String user_pos) {
        this.user_pos = user_pos;
    }

    public String getUser_plateno() {
        return user_plateno;
    }

    public void setUser_plateno(String user_plateno) {
        this.user_plateno = user_plateno;
    }


}