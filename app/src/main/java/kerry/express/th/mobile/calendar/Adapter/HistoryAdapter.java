package kerry.express.th.mobile.calendar.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kerry.express.th.mobile.calendar.Model.ListItem;
import kerry.express.th.mobile.calendar.R;

/**
 * Created by namwanta on 11/30/2016 AD.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private List<ListItem> listData;
    private LayoutInflater inflater;

    private HistoryAdapter.ItemClickCallback itemClickCallback;


    public HistoryAdapter(List<ListItem> listData, Context c) {
        inflater = LayoutInflater.from(c);
        this.listData = listData;
    }

    public void setListData(ArrayList<ListItem> exerciseList) {
        this.listData.clear();
        this.listData.addAll(exerciseList);
    }

    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item_history, parent, false);
        return new HistoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryAdapter.ViewHolder holder, int position) {
        ListItem item = listData.get(position);

        holder.txt_username.setText(item.getUser_name());
        holder.txt_userplateno.setText(item.getUser_plateno());

        holder.txt_tranwhat.setText(item.getTran_what());
        holder.txt_tranwhere.setText(item.getTran_where());
        holder.txt_tranhow.setText(item.getTran_how());
        holder.txt_trantype.setText(item.getTran_type());
        holder.txt_trandistance.setText(item.getTran_distance()+" กม.");
        holder.txt_tranamount.setText(item.getTran_amount()+" บาท");

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //// ประกาศค่าที่อยู่ใน row ของ recycleview
        View container;

        public TextView txt_username;
        public TextView txt_userplateno;

        public TextView txt_tranwhat;
        public TextView txt_tranwhere;
        public TextView txt_tranhow;
        public TextView txt_trantype;
        public TextView txt_trandistance;
        public TextView txt_tranamount;


        public ViewHolder(View itemView) {
            super(itemView);

            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_userplateno = (TextView) itemView.findViewById(R.id.txt_userplateno);

            txt_tranwhat = (TextView) itemView.findViewById(R.id.txt_tranwhat);
            txt_tranwhere = (TextView) itemView.findViewById(R.id.txt_tranwhere);
            txt_tranhow = (TextView) itemView.findViewById(R.id.txt_tranhow);
            txt_trantype = (TextView) itemView.findViewById(R.id.txt_trantype);
            txt_trandistance = (TextView) itemView.findViewById(R.id.txt_trandistance);
            txt_tranamount = (TextView) itemView.findViewById(R.id.txt_tranamount);
            container = (View) itemView.findViewById(R.id.cont_item_root);

        }
    }
    @Override
    public int getItemCount() {
        return listData.size();
    }


    public interface ItemClickCallback {
        void onItemClick(int p);

        void onSecondaryIconClick(int p);
    }

}
