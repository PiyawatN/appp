package kerry.express.th.mobile.calendar.Realm.Migrations;

import java.util.Date;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Created by namwanta on 11/22/2016 AD.
 */

public class RealmMigrations implements RealmMigration {


    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        RealmSchema schema = realm.getSchema();

        if (oldVersion == 0) {
            schema.create("UserDetail")
                    .addField("user_imei", String.class)
                    .addField("user_name", String.class)
                    .addField("user_dep", String.class)
                    .addField("user_pos", String.class)
                    .addField("user_plateno", String.class);

            schema.create("Transaction")
                    .addField("tran_where", String.class)
                    .addField("tran_what", String.class)
                    .addField("tran_how", String.class)
                    .addField("tran_type", String.class)
                    .addField("tran_amount", String.class)
                    .addField("tran_distance", String.class)
                    .addField("tran_date", Date.class);

            oldVersion++;
        }

        if (oldVersion == 1) {

            oldVersion++;
        }


    }
}