package kerry.express.th.mobile.calendar.HelpUsers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by namwanta on 11/28/2016 AD.
 */

public class UserHelper {

    Context context;
    SharedPreferences sharedPerfs;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPerfs1;
    SharedPreferences.Editor editor1;
    SharedPreferences sharedPerfs2;
    SharedPreferences.Editor editor2;

    // Prefs Keys
    static String perfsName = "UserHelper";
    static int perfsMode = 0;

    public UserHelper(Context context) {
        this.context = context;
        this.sharedPerfs = this.context.getSharedPreferences(perfsName, perfsMode);
        this.editor = sharedPerfs.edit();

        this.sharedPerfs1 = this.context.getSharedPreferences(perfsName, perfsMode);
        this.editor1 = sharedPerfs1.edit();

        this.sharedPerfs2 = this.context.getSharedPreferences(perfsName, perfsMode);
        this.editor2 = sharedPerfs2.edit();
    }

    public void createDay(String strDay) {
        editor1.putString("strDay", strDay);
        editor1.commit();
    }


    public String getDay() {
        return sharedPerfs1.getString("strDay", "0");
    }

    public void createDayintext(String strDayIT) {
        editor1.putString("strDayIT", strDayIT);
        editor1.commit();
    }


    public String getDayinttext() {
        return sharedPerfs2.getString("strDayIT", "0");
    }

    public void deleteDay() {
        editor2.clear();
        editor2.commit();
    }

    public void createSessionLogin() {
        editor.putBoolean("LoginStatus", true);
        editor.commit();
    }

    public void deleteSession() {
        editor.clear();
        editor.commit();
    }

    public boolean getLoginStatus() {
        return sharedPerfs.getBoolean("LoginStatus", false);
    }

}
