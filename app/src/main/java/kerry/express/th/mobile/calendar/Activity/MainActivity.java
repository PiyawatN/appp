package kerry.express.th.mobile.calendar.Activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import kerry.express.th.mobile.calendar.Fragment.CalendarFragment;
import kerry.express.th.mobile.calendar.HelpUsers.UserHelper;
import kerry.express.th.mobile.calendar.Pager.Adapter;
import kerry.express.th.mobile.calendar.R;
import kerry.express.th.mobile.calendar.Realm.Controller.RealmController;

public class MainActivity extends AppCompatActivity {
    private Adapter adapter;
    private static LinearLayout toolbar;

    private static ViewPager pager;
    int page;
    String status = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new Adapter(getSupportFragmentManager());
        RealmController.with(this).getInstance();  //ต้องมีทุกอัน
        UserHelper usrHelper = new UserHelper(this);

        CalendarFragment fragment = new CalendarFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id._viewPager, fragment);
        transaction.commit();

//        toolbar = (LinearLayout) findViewById(R.id.btn_back);
//        toolbar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.e("Page",pager.getCurrentItem()+"");
//                page = pager.getCurrentItem();
//                if (page == 1){
//
//                    Log.e("Click","GONE");
//                    toolbarcontrol(0);
//                }
//                pager.setCurrentItem(pager.getCurrentItem() - 1);
//            }
//        });


//        pager = (ViewPager) findViewById(R.id._viewPager);
//        pager.setAdapter(adapter);


//        Log.e("status", "" + usrHelper.getLoginStatus());
//        RealmController.test();
//        RealmController.getTest();
//        RealmController.testDelete();
//        RealmController.getMembers();


    }

//    public static void toolbarcontrol(int position) {
//        if (position == 0) {
//            toolbar.setVisibility(View.GONE);
//        }if (position == 1) {
//            toolbar.setVisibility(View.VISIBLE);
//        }
//    }
//
//    public static void swapFragment(int position) {
//        pager.setCurrentItem(position);
//    }
}
