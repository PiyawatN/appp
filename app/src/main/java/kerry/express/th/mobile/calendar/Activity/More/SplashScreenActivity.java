package kerry.express.th.mobile.calendar.Activity.More;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import kerry.express.th.mobile.calendar.Activity.Welcome;
import kerry.express.th.mobile.calendar.R;
import kerry.express.th.mobile.calendar.Realm.Controller.RealmController;

public class SplashScreenActivity extends AppCompatActivity {


    private Handler handler;
    private Runnable runnable;
    long delay_time;
    long time = 2000L;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        RealmController.with(this).getInstance(); //ประกาศinstance

        handler = new Handler();


        runnable = new Runnable() {
            public void run() {
                Intent intent = new Intent(SplashScreenActivity.this, Welcome.class);
                startActivity(intent);
                finish();
            }
        };






    }

    public void onResume() {
        super.onResume();
        delay_time = time;
        handler.postDelayed(runnable, delay_time);
        time = System.currentTimeMillis();
    }

    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
        time = delay_time - (System.currentTimeMillis() - time);
    }


}
