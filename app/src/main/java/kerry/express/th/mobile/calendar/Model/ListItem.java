package kerry.express.th.mobile.calendar.Model;

import java.util.Date;

/**
 * Created by namwanta on 11/30/2016 AD.
 */

public class ListItem {

    ///User////
    private String user_imei;
    private String user_name;
    private String user_dep;
    private String user_pos;
    private String user_plateno;

    ////Transaction////

    private String tran_where;
    private String tran_what;
    private String tran_how;
    private String tran_type;
    private String tran_amount;
    private String tran_distance;
    private Date tran_date;


    public String getUser_imei() {
        return user_imei;
    }

    public void setUser_imei(String user_imei) {
        this.user_imei = user_imei;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_dep() {
        return user_dep;
    }

    public void setUser_dep(String user_dep) {
        this.user_dep = user_dep;
    }

    public String getUser_pos() {
        return user_pos;
    }

    public void setUser_pos(String user_pos) {
        this.user_pos = user_pos;
    }

    public String getUser_plateno() {
        return user_plateno;
    }

    public void setUser_plateno(String user_plateno) {
        this.user_plateno = user_plateno;
    }

    public String getTran_where() {
        return tran_where;
    }

    public void setTran_where(String tran_where) {
        this.tran_where = tran_where;
    }

    public String getTran_what() {
        return tran_what;
    }

    public void setTran_what(String tran_what) {
        this.tran_what = tran_what;
    }

    public String getTran_how() {
        return tran_how;
    }

    public void setTran_how(String tran_how) {
        this.tran_how = tran_how;
    }

    public String getTran_type() {
        return tran_type;
    }

    public void setTran_type(String tran_type) {
        this.tran_type = tran_type;
    }

    public String getTran_amount() {
        return tran_amount;
    }

    public void setTran_amount(String tran_amount) {
        this.tran_amount = tran_amount;
    }

    public String getTran_distance() {
        return tran_distance;
    }

    public void setTran_distance(String tran_distance) {
        this.tran_distance = tran_distance;
    }

    public Date getTran_date() {
        return tran_date;
    }

    public void setTran_date(Date tran_date) {
        this.tran_date = tran_date;
    }
}
