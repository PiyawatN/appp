package kerry.express.th.mobile.calendar.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import kerry.express.th.mobile.calendar.Activity.HistoryActivity;
import kerry.express.th.mobile.calendar.R;

/**
 * Created by namwanta on 11/11/2016 AD.
 */

public class CalendarFragment extends Fragment {


    CalendarView calendar;
    String _year = "";
    String _day = "";
    String _month = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);
        calendar = (CalendarView) rootView.findViewById(R.id._calendar);
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {

                Log.e("D_TAG(CalendarF Click)", year + " " + (month + 1) + " " + dayOfMonth);

                _day = Integer.toString(dayOfMonth);
                _year = Integer.toString(year);
                _month = Integer.toString(month + 1);
                try {
//                    BusStation.getBus().post(new Message(_day + "-" +
//                            _month + "-" + _year));
                    Log.e("Click","SHOW btn back");
                    Intent newActivity = new Intent(getActivity(), HistoryActivity.class);
                    newActivity.putExtra("DD",_day);
                    newActivity.putExtra("MM",_month);
                    newActivity.putExtra("YYYY",_year);

                    startActivity(newActivity);
                    getActivity().finish();
//                    MainActivity.toolbarcontrol(1);
//                    MainActivity.swapFragment(1);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


            }

        });
        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
//        MainActivity.toolbarcontrol(1);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }


}
