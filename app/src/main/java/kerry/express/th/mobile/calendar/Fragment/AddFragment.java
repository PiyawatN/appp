package kerry.express.th.mobile.calendar.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.squareup.otto.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import kerry.express.th.mobile.calendar.Activity.HistoryActivity;
import kerry.express.th.mobile.calendar.Adapter.HistoryAdapter;
import kerry.express.th.mobile.calendar.HelpUsers.BusStation;
import kerry.express.th.mobile.calendar.HelpUsers.Message;
import kerry.express.th.mobile.calendar.HelpUsers.UserHelper;
import kerry.express.th.mobile.calendar.R;
import kerry.express.th.mobile.calendar.Realm.Controller.RealmController;

/**
 * Created by namwanta on 11/15/2016 AD.
 */

public class AddFragment extends Fragment {


    private Handler handler;
    private Runnable runnable;
    long delay_time;
    long time = 10L;

    String Tran_where, Tran_what, Tran_how, Tran_type, Tran_amount, Tran_distance;
    String Tran_date;
    Date Save_date;
    EditText _where, _what, _amount, _distance;
    RadioGroup rg_how, rg_type;
    Button btn_save;

    public static HistoryAdapter adapter;
    public static ArrayList listData;

    //    SimpleDateFormat format = new SimpleDateFormat("DD-MM-YYYY");
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add, container, false);
        RealmController.with(getActivity()).getInstance();

        UserHelper usrHelper = new UserHelper(getActivity());

        _where = (EditText) rootView.findViewById(R.id._where);
        _what = (EditText) rootView.findViewById(R.id._what);
        _amount = (EditText) rootView.findViewById(R.id._amount);
        _distance = (EditText) rootView.findViewById(R.id._distance);
        rg_how = (RadioGroup) rootView.findViewById(R.id.rg_how);
        rg_type = (RadioGroup) rootView.findViewById(R.id.rg_type);
        btn_save = (Button) rootView.findViewById(R.id.btn_save);


        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        Tran_where = _where.getText().toString();
                        Tran_what = _what.getText().toString();
                        Tran_amount = _amount.getText().toString();
                        Tran_distance = _distance.getText().toString();

                        //Check Radio
                        switch (rg_how.getCheckedRadioButtonId()) {
                            case R.id.rb_car:
                                Tran_how = "รถส่วนตัว";
                                break;
                            case R.id.rb_taxi:
                                Tran_how = "รถแท็กซี่";
                                break;
                            case R.id.rb_mrt:
                                Tran_how = "รถไฟฟ้า";
                                break;
                        }

                        switch (rg_type.getCheckedRadioButtonId()) {
                            case R.id.rb_go:
                                Tran_type = "ไป";
                                break;
                            case R.id.rb_goback:
                                Tran_type = "ไป - กลับ";
                                break;
                            case R.id.rb_back:
                                Tran_type = "กลับ";
                                break;
                        }

                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            Save_date = format.parse(Tran_date);
                            Log.e("Date_save", Save_date + "");
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        RealmController.insertTransaction(Tran_where, Tran_what, Tran_how, Tran_type, Tran_amount, Tran_distance, Save_date);
                        Log.e("Check save", Tran_where + "\n" + Tran_what + "\n" + Tran_how + "\n" + Tran_type + "\n" + Tran_amount + "\n" + Tran_distance + "\n" + Save_date);

                        _where.setText("");
                        _what.setText("");
                        _amount.setText("");
                        _distance.setText("");
                        rg_how.check(R.id.rb_car);
                        rg_type.check(R.id.rb_go);

                        HistoryActivity.swapFragment(0);

                    }
                }, time);

            }
        });
        ////////////////////////////////////

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        BusStation.getBus().register(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        BusStation.getBus().unregister(this);


    }

    @Subscribe
    public void recievedMessage(Message message) {
        Tran_date = message.getMsg();
        Log.e("(Add_F) Check date", Tran_date);
//        try {
//            Tran_date = format.parse(message.getMsg());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }
}
