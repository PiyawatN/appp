package kerry.express.th.mobile.calendar.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import kerry.express.th.mobile.calendar.Activity.HistoryActivity;
import kerry.express.th.mobile.calendar.Adapter.HistoryAdapter;
import kerry.express.th.mobile.calendar.HelpUsers.BusStation;
import kerry.express.th.mobile.calendar.HelpUsers.Message;
import kerry.express.th.mobile.calendar.HelpUsers.UserHelper;
import kerry.express.th.mobile.calendar.Model.ListItem;
import kerry.express.th.mobile.calendar.R;
import kerry.express.th.mobile.calendar.Realm.Controller.RealmController;


public class HistoryFragment extends Fragment {
    TextView date;
    String date_show = "2016-11-15";
    FloatingActionButton add, f5;
    String DMY = "";
    String data;


    public static RecyclerView recView;
    public static HistoryAdapter adapter;
    public static ArrayList listData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RealmController.with(getActivity()).getInstance();  //ต้องมีทุกอัน

        UserHelper usrHelper = new UserHelper(getActivity());
        date_show = usrHelper.getDay();
        DMY = usrHelper.getDayinttext();

        Log.e("check day!!(His_F)", date_show);
        Log.e("check day2!!(His_F)", DMY);

        RealmController.getHistory(date_show);


        View rootView = inflater.inflate(R.layout.fragment_history, container, false);
        date = (TextView) rootView.findViewById(R.id._date);
        date.setText(DMY);


        add = (FloatingActionButton) rootView.findViewById(R.id._add);
        add.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                try {

                    BusStation.getBus().post(new Message(date_show));
                    BusStation.getBus().post(date_show);
                    HistoryActivity.swapFragment(1);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });


        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recView = (RecyclerView) getActivity().findViewById(R.id.rec_list);
        recView.setHasFixedSize(true);

        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        recView.setLayoutManager(lm);


        listData = (ArrayList) RealmController.getHistoryList(date_show);

//        Log.e("ListData", String.valueOf(listData));

        adapter = new HistoryAdapter(listData, getActivity());
        recView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(createHelperCallback());
        itemTouchHelper.attachToRecyclerView(recView);


    }



    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
                new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        moveItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                        return true;
                    }

                    @Override
                    public void onSwiped(final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                        deleteItem(viewHolder.getAdapterPosition());
                    }

                    @Override
                    public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                        if (viewHolder instanceof RecyclerView.ViewHolder) return 0;
                        return super.getSwipeDirs(recyclerView, viewHolder);
                    }
                };
        return simpleItemTouchCallback;
    }

    private void moveItem(int oldPos, int newPos) {

        ListItem item = (ListItem) listData.get(oldPos);
        Log.e("Position: ", "OLD " + oldPos + " NEW " + newPos);
        listData.remove(oldPos);
        listData.add(newPos, item);
        adapter.notifyItemMoved(oldPos, newPos);


    }

    private void deleteItem(final int position) {
        listData.remove(position);
        adapter.notifyItemRemoved(position);
    }

    public void FunctionRecycleview(){
        UserHelper usrHelper = new UserHelper(getActivity());
        date_show = usrHelper.getDay();

        RealmController.getHistory(date_show);
    }
    @Override
    public void onResume() {
        super.onResume();
        listData = (ArrayList) RealmController.getHistoryList(date_show);
        Log.e("recume", "resume");
        recView = (RecyclerView) getActivity().findViewById(R.id.rec_list);
        recView.setHasFixedSize(true);

        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        recView.setLayoutManager(lm);

        adapter = new HistoryAdapter(listData, getActivity());
        recView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


//        RealmController.getHistory(Save_date);
//        BusStation.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        listData = (ArrayList) RealmController.getHistoryList(date_show);
        Log.e("pause", "ok");
        recView = (RecyclerView) getActivity().findViewById(R.id.rec_list);
        recView.setHasFixedSize(true);

        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        recView.setLayoutManager(lm);

        adapter = new HistoryAdapter(listData, getActivity());
        recView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

//        RealmController.getHistory(Save_date);
//        BusStation.getBus().unregister(this);
    }


}
