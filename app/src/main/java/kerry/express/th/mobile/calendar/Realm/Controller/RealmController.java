package kerry.express.th.mobile.calendar.Realm.Controller;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.content.Context;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import kerry.express.th.mobile.calendar.Model.ListItem;
import kerry.express.th.mobile.calendar.Realm.Table.Transaction;
import kerry.express.th.mobile.calendar.Realm.Table.UserDetail;

/**
 * Created by namwanta on 11/22/2016 AD.
 */

public class RealmController {

    private static RealmController instance;
    private static Realm realm;
    public static Context mContext;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }


    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    public static void getRealmInstance() {
        RealmConfiguration realmConfig = new RealmConfiguration.Builder().build();
        Realm bgRealm = Realm.getInstance(realmConfig);
    }


    /////////////////////////////////////////////////////////////////


    public static void insertUser(final String _imei, final String _name, final String _dep,
                                  final String _pos, final String _plateno) {

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {

                UserDetail userdetail = bgRealm.createObject(UserDetail.class);
                userdetail.setUser_imei(_imei);
                userdetail.setUser_name(_name);
                userdetail.setUser_dep(_dep);
                userdetail.setUser_pos(_pos);
                userdetail.setUser_plateno(_plateno);
                Log.e("insert:", "Success first");

            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Log.e("insert(User):", "succesful.");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e("insert(User):", "not success");
            }
        });
    }

    public static void insertTransaction(final String _where, final String _what, final String _how,
                                         final String _type, final String _amount, final String _distance, final Date _date) {

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {

                Transaction transaction = bgRealm.createObject(Transaction.class);
                transaction.setTran_where(_where);
                transaction.setTran_what(_what);
                transaction.setTran_how(_how);
                transaction.setTran_type(_type);
                transaction.setTran_amount(_amount);
                transaction.setTran_distance(_distance);
                transaction.setTran_date(_date);

                Log.e("insert:", "Success first");

            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Log.e("insert(Transaction):", "succesful.");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e("insert(Transaction):", "not success");
            }
        });
    }


    public static void getMembers() {

        UserDetail user = realm.where(UserDetail.class).findFirst();
        Log.e("EIIE", user.getUser_imei() + " " + user.getUser_name() + " " + user.getUser_dep() + " " + user.getUser_pos() + " " + user.getUser_plateno());
    }


    //insert
    public static void testbeta() {

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {

                UserDetail userdetail = bgRealm.createObject(UserDetail.class);
                userdetail.setUser_imei("1234");
                userdetail.setUser_name("benz");
                userdetail.setUser_dep("RD");
                userdetail.setUser_pos("mobile");
                userdetail.setUser_plateno("290");
                Log.e("insert:", "Success first");
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Log.e("TD)insert:", "Success");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e("(FD)insert:", "not success");
            }
        });

    }


    // query
    public static void getTest() {

        RealmResults<UserDetail> user = realm.where(UserDetail.class).findAll();
//            Log.e("EIIE", String.valueOf(user));// ทั้งหมดที่มี

        UserDetail aa = realm.where(UserDetail.class).findFirst();
//        Log.e("Name....(getTest)", aa.getUser_name());

        for (UserDetail u : user) { // วนลูปแล้วเอาออกมาทั้งหมด
            Log.e("Yeah !!!(gettest)", String.valueOf(u));
        }


//        RealmResults<UserDetail> user = realm.where(UserDetail.class)
//                .equalTo("name", "John")
//                .or()
//                .equalTo("name", "Peter")
//                .findAllAsync();


    }

    public static void getHistory(String date) {
//
//        Transaction aa = realm.where(Transaction.class).findFirst();
//        Log.e("tran_date", String.valueOf(aa.getTran_date()));
//
//        RealmResults<Transaction> tran = realm.where(Transaction.class).findAll();
//        Transaction a = realm.where(Transaction.class).findFirst();
//        Log.e("Resuals !!!!!", String.valueOf(tran));
//
//        for (Transaction t : tran) {
//            Log.e("T !!!", String.valueOf(t));
//        }

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        Date Test = null;
        try {
            Test = dateformat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        RealmResults<Transaction> tran = realm.where(Transaction.class)
                .equalTo("tran_date", Test)
                .findAll();
        Log.e("Success #$%@#$@ !!!", String.valueOf(tran));
        Log.e("size", String.valueOf(tran.size()));
//
        //////////////
//
//        RealmResults<Transaction> tranData = realm.where(Transaction.class)
//                .findAll();
////        Log.e("Date send" , date);
////        Log.e("Data", String.valueOf(tranData));
//
//        for (Transaction trans : tranData) {
//            Date dateinrealm = trans.getTran_date();
//            String dateinput = date;
//            String datetime1 = dateformat.format(dateinrealm);
////            Log.e("Current Date Time1 : ", datetime1);
////            Log.e("Current Date Time2 : ", dateinput);
//
//            if (dateinput.equalsIgnoreCase(datetime1)) {
////                Log.e("Result !!!!!!!!!!!! : ", String.valueOf(trans));
//            }
//        }
    }


    public static List<ListItem> getHistoryList(String date) {
        List<ListItem> data = new ArrayList<>();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        Date His_date = null;
        try {
            His_date = dateformat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        for (Transaction transactions : realm.where(Transaction.class)
                .equalTo("tran_date",His_date)
                .findAll()) {

            ListItem item = new ListItem();
            item.setTran_where(transactions.getTran_where());
            item.setTran_what(transactions.getTran_what());
            item.setTran_how(transactions.getTran_how());
            item.setTran_amount(transactions.getTran_amount());
            item.setTran_distance(transactions.getTran_distance());
            item.setTran_type(transactions.getTran_type());

            UserDetail aa = realm.where(UserDetail.class).findFirst();
            item.setUser_name(aa.getUser_name());
            item.setUser_plateno(aa.getUser_plateno());

            data.add(item);

//            Log.e("data add", String.valueOf(data));
        }


        return data;
    }


    ////delete
    public static void testDelete() {
        RealmResults<UserDetail> result = realm.where(UserDetail.class).findAll();
        result.get(0).deleteFromRealm();
    }


}