package kerry.express.th.mobile.calendar.Application;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import kerry.express.th.mobile.calendar.Realm.Migrations.RealmMigrations;
import kerry.express.th.mobile.calendar.Unilities.Contextor;

/**
 * Created by namwanta on 11/29/2016 AD.
 */

public class MainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());


        Contextor.getInstance().init(getApplicationContext());

        try {

            Realm.init(getApplicationContext());
            RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                    .name("Calendar")
                    .schemaVersion(1) // Must be bumped when the schema changes
                    .migration(new RealmMigrations()) // Migration to run instead of throwing an exception
                    .build();
            Realm.getInstance(realmConfiguration);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}

