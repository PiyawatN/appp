package kerry.express.th.mobile.calendar.Activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import kerry.express.th.mobile.calendar.Fragment.HistoryFragment;
import kerry.express.th.mobile.calendar.HelpUsers.BusStation;
import kerry.express.th.mobile.calendar.HelpUsers.Message;
import kerry.express.th.mobile.calendar.HelpUsers.UserHelper;
import kerry.express.th.mobile.calendar.Pager.Adapter;
import kerry.express.th.mobile.calendar.R;
import kerry.express.th.mobile.calendar.Realm.Controller.RealmController;

public class HistoryActivity extends AppCompatActivity {
    private Adapter adapter;
    private static LinearLayout toolbar;
    Date stringtodate;
    String day, month, year, result, datetostring;
    String ShowdateinText = "";
    UserHelper usrHelper;

    private static ViewPager pager;
    int page;
    String status = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        RealmController.with(this).getInstance();  //ต้องมีทุกอัน
        usrHelper = new UserHelper(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            day = bundle.getString("DD");
            month = bundle.getString("MM");
            year = bundle.getString("YYYY");
        }

        result = String.format("%s-%s-%s",
                year, month, day);


        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatdateshow = new SimpleDateFormat("EEE, MMM d, yyyy");

        try {
            stringtodate = format.parse(result);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        Log.e("before" , String.valueOf(stringtodate));
        datetostring = format.format(stringtodate);
        ShowdateinText = formatdateshow.format(stringtodate);
//        Log.e("Chang D to S",datetostring);
        usrHelper.createDay(datetostring);
        usrHelper.createDayintext(ShowdateinText);
        Log.e("Check Re.....(His_A)", datetostring);
//        RealmController.getHistory(result);
        BusStation.getBus().post(new Message(result));
        BusStation.getBus().post(result);

        adapter = new Adapter(getSupportFragmentManager());
        UserHelper usrHelper = new UserHelper(this);

        toolbar = (LinearLayout) findViewById(R.id.btn_back);
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.e("Page", pager.getCurrentItem() + "");
                page = pager.getCurrentItem();
                if (page == 0) {
                    Intent newActivity = new Intent(HistoryActivity.this, MainActivity.class);
                    startActivity(newActivity);
                    finish();
//                    Log.e("Click","GONE");
//                    toolbarcontrol(0);
                }
                pager.setCurrentItem(pager.getCurrentItem() - 1);
            }
        });


        pager = (ViewPager) findViewById(R.id._viewPager);
        pager.setAdapter(adapter);


    }


    public void functionRefresh() {

        HistoryFragment.listData = (ArrayList) RealmController.getHistoryList(datetostring);

    }

    public static void toolbarcontrol(int position) {
        if (position == 0) {
            toolbar.setVisibility(View.GONE);
        }
        if (position == 1) {
            toolbar.setVisibility(View.VISIBLE);
        }
    }

    public static void swapFragment(int position) {
        pager.setCurrentItem(position);
    }


}
