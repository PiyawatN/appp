package kerry.express.th.mobile.calendar.Pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import kerry.express.th.mobile.calendar.Fragment.AddFragment;
import kerry.express.th.mobile.calendar.Fragment.HistoryFragment;

public class Adapter extends FragmentStatePagerAdapter {
    private final int PAGE_NUM = 2;

    public Adapter(FragmentManager fm) {
        super(fm);
    }

    @Override
        public Fragment getItem(int position) {

        if (position == 0){
            return new HistoryFragment();}
        else if (position == 1){
            return new AddFragment();}
//        else if (position == 2){
//            return new AddFragment();}
        return null;

    }

    @Override
    public int getCount() {
        return PAGE_NUM;
    }
}

