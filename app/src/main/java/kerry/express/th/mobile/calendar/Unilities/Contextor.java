package kerry.express.th.mobile.calendar.Unilities;

import android.content.Context;

/**
 * Created by namwanta on 11/22/2016 AD.
 */

public class Contextor {

    private static Contextor instance;
    private Context context;

    public static Contextor getInstance(){
        if (instance == null)
            instance = new Contextor();
        return instance;
    }

    private Context mContext;

    private Contextor() {

    }

    public  void init(Context context){
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

}