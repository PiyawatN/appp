package kerry.express.th.mobile.calendar.HelpUsers;

/**
 * Created by namwanta on 11/17/2016 AD.
 */

public class Message {
    private String msg;

    public Message(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
