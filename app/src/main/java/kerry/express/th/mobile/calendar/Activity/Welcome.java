package kerry.express.th.mobile.calendar.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import kerry.express.th.mobile.calendar.HelpUsers.UserHelper;
import kerry.express.th.mobile.calendar.R;
import kerry.express.th.mobile.calendar.Realm.Controller.RealmController;

public class Welcome extends AppCompatActivity {
    Realm realm;
    RealmConfiguration realmConfiguration;


    final private int REQUEST_CODE_ASK_PERMISSIONS_IMEI = 11;
    final private int REQUEST_CODE_ASK_PERMISSIONS_LOCATION = 22;
    final private int REQUEST_CODE_ASK_PERMISSIONS_CAMERA = 33;
    final private int REQUEST_CODE_ASK_PERMISSIONS_STORAGE = 44;
    TextView imei_number;
    String user_imei, user_name, user_dep, user_pos, user_plateno, Status;
    EditText _name, _dep, _pos, _plateno;
    Button btn_save;
    UserHelper usrHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        RealmController.with(this).getInstance();
        usrHelper = new UserHelper(this);
        Status = (""+usrHelper.getLoginStatus());
        Log.e("Status !!!!(Welcome)",""+ Status);

        addImeiPermission();
        //Get Login Status
        if (usrHelper.getLoginStatus()) {
            Intent newActivity = new Intent(Welcome.this, MainActivity.class);
            startActivity(newActivity);
            finish();
        }


        findViewById();


        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
                Intent newActivity = new Intent(Welcome.this, MainActivity.class);
                startActivity(newActivity);
                finish();
            }
        });

    }

    public void save(){
        user_name = _name.getText().toString();
        user_dep = _dep.getText().toString();
        user_pos = _pos.getText().toString();
        user_plateno = _plateno.getText().toString();
        user_imei = imei(getApplicationContext());
//        imei_number.setText(""+user_imei);

        //  RealmController.InsertUser(user_imei,user_name,user_dep,user_pos,user_plateno);
//        Toast.makeText(Welcome.this,
//                user_imei+"\n"+user_name+"\n"+user_dep+"\n"+user_pos+"\n"+user_plateno+"\n"
//                , Toast.LENGTH_LONG).show();

        RealmController.insertUser(user_imei, user_name, user_dep, user_pos, user_plateno);
        usrHelper.createSessionLogin();
        Log.e("imei",user_imei);
        Log.e("name",user_name);
        Log.e("dep",user_dep);
        Log.e("pos",user_pos);
        Log.e("no",user_plateno);

    }

    public void findViewById(){
        btn_save = (Button)findViewById(R.id._save);
//        imei_number = (TextView)findViewById(R.id._view);
        _name = (EditText)findViewById(R.id._name);
        _dep = (EditText)findViewById(R.id._dep);
        _pos = (EditText)findViewById(R.id._pos);
        _plateno = (EditText)findViewById(R.id._plateno);

    }


    public static String imei(Context context) {
//        mContext = Contextor.getInstance().getContext();
        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }
    private void addImeiPermission() {
        int hasWriteContactsPermission = ActivityCompat.checkSelfPermission(Welcome.this, Manifest.permission.READ_PHONE_STATE);

        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Welcome.this, new String[]{Manifest.permission.READ_PHONE_STATE},
                    REQUEST_CODE_ASK_PERMISSIONS_IMEI);
            return;
        }
    }
    private void addLocationPermission() {
        int hasWriteContactsPermission = ActivityCompat.checkSelfPermission(Welcome.this, Manifest.permission.ACCESS_COARSE_LOCATION);

        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Welcome.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_CODE_ASK_PERMISSIONS_LOCATION);
            return;
        }
    }
    private void addCameraPermission() {
        int hasWriteContactsPermission = ActivityCompat.checkSelfPermission(Welcome.this, Manifest.permission.CAMERA);

        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Welcome.this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CODE_ASK_PERMISSIONS_CAMERA);
            return;
        }
    }
    private void addStoragePermission() {
        int hasWriteContactsPermission = ActivityCompat.checkSelfPermission(Welcome.this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Welcome.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE_ASK_PERMISSIONS_STORAGE);
            return;
        }
    }

}
